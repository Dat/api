import * as express from 'express';
import { Request, Response } from 'express';
const app = express();
const http = require('http');
const https = require('https');

http.createServer(app).listen(3000);
https.createServer(app).listen(80);

const {
    PORT = 3000,
} = process.env;

app.get('/', (req: Request, res: Response) => {
    res.send({
        "data": {
            "organization": {
                "branding": {
                    "__typename": "Branding",
                    "primaryColor": "#fff",
                    "iconImageURL": "https://kidsloop-alpha-account-asset-objects.s3.ap-northeast-2.amazonaws.com/organizations/0ee01c37-c014-4c22-bb81-84d4f2a53b36/2021-06-28/icon/croppedImage"
                }
            }
        }
    });
});

if (require.main === module) { // true if file is executed
    // app.listen(PORT, () => {
    //     console.log('server started at http://localhost:'+PORT);
    // });

    app.listen(() => {
        var server = http.createServer(this);
        console.log('server started at http://localhost:'+PORT);
        return server.listen.apply(server);
    });
}

export default app;